package com.example.MonopolyBackend.controllers;

import com.example.MonopolyBackend.exceptions.GameNotFoundException;
import com.example.MonopolyBackend.exceptions.UserNotFoundException;
import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.User;
import com.example.MonopolyBackend.security.CustomAuthenticationManager;
import com.example.MonopolyBackend.security.JwtTokenProvider;
import com.example.MonopolyBackend.services.GameService;
import com.example.MonopolyBackend.services.UserService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserService userService;
    @Autowired
    private GameService gameService;
    @Autowired
    private CustomAuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenProvider tokenProvider;
    @Autowired
    private GameController gameController;

    private User user;
    private Gson gson;
    private String jwt;

    @Before
    public void initialise() {
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        user = new User("test: " + LocalDateTime.now().toString().substring(0, 20), "test@test.test", "testtest", false);
        user = userService.createUser(user);

        UsernamePasswordAuthenticationToken u = new UsernamePasswordAuthenticationToken(user.getUsername(), "testtest");

        Authentication authentication = authenticationManager.authenticate(u);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        try {
            jwt = tokenProvider.generateToken(authentication);
        } catch (UserNotFoundException e) {
            System.out.println("User not found");
        }
    }

    @Test
    public void createNewGame() throws GameNotFoundException {
        Game game = new Game();
        game = gameService.save(game);
        try {
            gameController.createNewGame(game.getId() + "");
        } catch (IOException | InvalidFormatException | GameNotFoundException e) {
            e.printStackTrace();
        }
        Game newGame = gameService.findGameById(game.getId());
        assert newGame.isStarted();
        assert newGame.getFields().size() == 40;
        assert newGame.getCards().size() == 28;
    }

    @Test
    public void createNewLobby() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        byte[] json = new byte[0];
        json = user.getUsername().getBytes();


        this.mockMvc.perform(post("/api/lobby/create")
                .contentType(new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8")))
                .header("Authorization", "Bearer " + jwt)
                .content(json))
                .andExpect(status().isCreated());
    }

    @Test
    public void addPlayer() throws GameNotFoundException {
        Game game = new Game();
        game = gameService.save(game);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("gameId",game.getId());
            jsonObject.put("userId",user.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            gameController.addPlayer(jsonObject.toString());
        } catch (GameNotFoundException | UserNotFoundException e) {
            e.printStackTrace();
        }

        assert gameService.findGameById(game.getId()).getPlayers().size() == 1;
    }
}
