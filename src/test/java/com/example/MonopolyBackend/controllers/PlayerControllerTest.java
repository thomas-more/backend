package com.example.MonopolyBackend.controllers;

import com.example.MonopolyBackend.exceptions.GameNotFoundException;
import com.example.MonopolyBackend.exceptions.PlayerNotFoundException;
import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.User;
import com.example.MonopolyBackend.services.GameService;
import com.example.MonopolyBackend.services.PlayerService;
import com.example.MonopolyBackend.services.UserService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PlayerControllerTest {

     @Autowired
     private GameService gameService;
     @Autowired
     private UserService userService;
     @Autowired
     private PlayerService playerService;
     @Autowired
     private PlayerController playerController;

    private User user;

    @Before
    public void initialise() {
        user = new User("test: " + LocalDateTime.now().toString().substring(0, 20), "test@test.test", "testtest", false);
        user = userService.createUser(user);
    }
    @Test
    public void pickPawn() throws GameNotFoundException {
        Player player = new Player(user.getUsername(),null);
        Game game = new Game(player);
        game = gameService.save(game);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("gameId",game.getId());
            jsonObject.put("pawn","horse");
            jsonObject.put("playerId",player.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            playerController.pickPawn(jsonObject.toString());
        } catch (GameNotFoundException | PlayerNotFoundException e) {
            e.printStackTrace();
        }

        assert gameService.findGameById(game.getId()).getPlayers().get(0).getPawn().equalsIgnoreCase("horse");
    }
}