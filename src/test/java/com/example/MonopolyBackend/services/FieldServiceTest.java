package com.example.MonopolyBackend.services;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import com.example.MonopolyBackend.model.fields.StreetField;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Thomas Somers
 * @version 1.0 10/03/2019 14:08
 */
public class FieldServiceTest {

    @Test
    public void receivePlayer() {
        List<Player> players = new ArrayList<>();
        players.add(new Player(5, "test", "hoed"));
        Game game = new Game(players);
        Player player1 = new Player(1, "player1", "paard");
        Player player2 = new Player(2, "player2", "auto");
        StreetField field = new StreetField("straat",1, new Street(1, "borgerhout", "brown", 2), game, 500, 250, false, player1, 1000, 1000, 1000, 100, 10000, 10000, 100000,0);
        field.receivePlayer(player2);
        assertEquals(player2.getMoney(), 15000 - field.getRentUnbuild());
        assertEquals(player1.getMoney(), 15000 + field.getRentUnbuild());
    }
}