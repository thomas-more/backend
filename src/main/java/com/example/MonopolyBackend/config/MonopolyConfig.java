package com.example.MonopolyBackend.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.ConnectionFactoryLocator;

@Configuration
public class MonopolyConfig {
        @Bean
        public ModelMapper modelMapper(){
            return  new ModelMapper();
        }
}
