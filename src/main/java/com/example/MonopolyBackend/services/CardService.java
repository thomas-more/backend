package com.example.MonopolyBackend.services;

import com.example.MonopolyBackend.model.cards.Card;
import com.example.MonopolyBackend.repositories.CardRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CardService {
    private final CardRepository cardRepository;

    public CardService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    public void saveAll(List<Card> cards) {
        cardRepository.saveAll(cards);
    }

}

