package com.example.MonopolyBackend.services;

import com.example.MonopolyBackend.exceptions.InviteNotFoundException;
import com.example.MonopolyBackend.model.Invite;
import com.example.MonopolyBackend.model.User;
import com.example.MonopolyBackend.repositories.InviteRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

@Service
@Transactional
public class InviteService {
    private final InviteRepository inviteRepository;

    public InviteService(InviteRepository inviteRepository) {
        this.inviteRepository = inviteRepository;
    }

    public Invite findById(int id) throws InviteNotFoundException {
        Optional<Invite> invite = inviteRepository.findById(id);
        if (invite.isPresent()) {
            return invite.get();
        }
        throw new InviteNotFoundException();
    }

    public Collection<Invite> getInvites(User user){
        return inviteRepository.findAllByUser(user);
    }

    public Invite save(Invite invite) {
        return inviteRepository.save(invite);
    }
}
