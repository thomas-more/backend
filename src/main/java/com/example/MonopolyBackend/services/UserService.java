package com.example.MonopolyBackend.services;

import com.example.MonopolyBackend.exceptions.UserNotFoundException;
import com.example.MonopolyBackend.model.User;
import com.example.MonopolyBackend.repositories.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;


@Service
public class UserService{

    private PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    public UserService(PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    public Collection<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUser(long id) throws UserNotFoundException {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        } else {
            throw new UserNotFoundException("User not found with id:" + id);
        }
    }

    public User getUser(String username) throws UserNotFoundException {
        System.out.println(username);
        Optional<User> optionalUser = Optional.ofNullable(userRepository.findByUsernameIgnoreCase(username));
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        } else {
            throw new UserNotFoundException("User not found with username:" + username);
        }
    }

    public User save(User user) {

        return userRepository.save(user);
    }

    public User createUser(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        this.save(user);
        return user;
    }

    public void confirmUser(long id) throws UserNotFoundException {
        User user = getUser(id);
        user.setConfirmed(true);
        userRepository.save(user);
    }

    public boolean userExists(String username){
        return userRepository.existsUserByUsername(username);
    }

    public User getUserByEmail(String email) throws UserNotFoundException {
        Optional<User> optionalUser = Optional.ofNullable(userRepository.findByEmail(email));
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        } else {
            throw new UserNotFoundException("User not found with email:" + email);
        }
    }

    public User changeUser(User user) throws UserNotFoundException {

        User returnUser = getUser(user.getId());
        returnUser.setUsername(user.getUsername());
        returnUser.setEmail(user.getEmail());

        if (!(user.getPassword().equals(returnUser.getPassword()))){
            returnUser.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        save(returnUser);
        return returnUser;

    }

    public boolean userCheckDuplicate(User user){
        if (userRepository.findByUsernameIgnoreCase(user.getUsername()) != null && userRepository.findByUsernameIgnoreCase(user.getUsername()).getId() != user.getId()){
            return true;
        }else{
            return false;
        }
    }


}