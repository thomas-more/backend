package com.example.MonopolyBackend.services;

import com.example.MonopolyBackend.exceptions.GameNotFoundException;
import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.fields.Field;
import com.example.MonopolyBackend.model.fields.PropertyField;
import com.example.MonopolyBackend.repositories.GameRepository;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @author Thomas Somers
 * @version 1.0 13/02/2019 8:53
 */

@Service
@Transactional
public class GameService {
    private final GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Game save(Game game) {
        return gameRepository.save(game);
    }

    public Game findGameById(int id) throws GameNotFoundException {
        Optional<Game> game = gameRepository.findById(id);
        if (game.isPresent()) {
            return game.get();
        }
        throw new GameNotFoundException();
    }

    public Game setNextPlayerPlaying(int gameId) throws GameNotFoundException {
        Game game = this.findGameById(gameId);
        int index = game.getPlayers().indexOf(game.getPlayerPlaying()); //index van huidige playerPlaying
        if (game.getNrPlayers() - 1 == index) {
            game.setPlayerPlaying(game.getPlayers().get(0));
        } else {
            game.setPlayerPlaying(game.getPlayers().get(++index));
        }
        return this.save(game);
    }

    public Game setFieldOwner(Game game) {
        for (Field f :game.getFields()) {
            if (f.getPosition()==game.getPlayerPlaying().getPosition()){
                PropertyField propertyField = (PropertyField) f;
                propertyField.setOwnedBy(game.getPlayerPlaying());
            }
        }
        return game;
    }
}
