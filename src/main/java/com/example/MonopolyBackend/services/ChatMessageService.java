package com.example.MonopolyBackend.services;

import com.example.MonopolyBackend.model.ChatMessage;
import com.example.MonopolyBackend.repositories.ChatMessageRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ChatMessageService {
        private final ChatMessageRepository chatMessageRepository;

    public ChatMessageService(ChatMessageRepository chatMessageRepository) {
        this.chatMessageRepository = chatMessageRepository;
    }
    public ChatMessage save(ChatMessage chatMessage){
        return chatMessageRepository.save(chatMessage);
    }



}
