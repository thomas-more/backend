package com.example.MonopolyBackend.services;

import com.example.MonopolyBackend.exceptions.FieldNotFoundException;
import com.example.MonopolyBackend.exceptions.InsufficientMoneyException;
import com.example.MonopolyBackend.exceptions.PlayerNotFoundException;
import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import com.example.MonopolyBackend.model.fields.Field;
import com.example.MonopolyBackend.model.fields.PropertyField;
import com.example.MonopolyBackend.model.fields.StreetField;
import com.example.MonopolyBackend.repositories.PlayerRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Transactional
public class PlayerService {

    private PlayerRepository playerRepository;
    private Random random;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
        this.random = new Random();
    }

    public Player save(Player player) {
        return this.playerRepository.save(player);
    }

    public Player getPlayer(int id) throws PlayerNotFoundException {
        Optional<Player> player = this.playerRepository.findById(id);
        if (player.isPresent()) {
            return player.get();
        }
        throw new PlayerNotFoundException();
    }

    private void passStart(Player playerplaying) {
        playerplaying.increaseMoney(2000);
    }

    public Player setPawn(int id, String pawn) throws PlayerNotFoundException {
        Player player = this.getPlayer(id);
        player.setPawn(pawn);
        return this.save(player);
    }

    public Player movePlayer(Player p) {
        boolean thrownDouble = false;
        if (!p.isInJail()) {
            int die1 = random.nextInt(6) + 1;
            int die2 = random.nextInt(6) + 1;
            int diceRoll = die1 + die2;
            int newPosition = p.getPosition() + diceRoll;
            Player playerOut = setPosition(p, newPosition);

            return this.save(playerOut);
        } else{

        }
        return p;
    }

    /*
    public Player prisonRoll(Player player, Game game) {
        if (player.isInJail()) {
            int die1 = random.nextInt(6) + 1;
            int die2 = random.nextInt(6) + 1;
            if (die1 == die2) {
                int roll = die1 + die2;
                player.setInJail(false);
                game.getPrison().remove(player);
                setPosition(player, roll);
            } else {
                game.getPrison().put(player, game.getPrison().get(player) - 1);
            }

        }
        return this.save(player);

    }
    */

    private Player setPosition(Player player, int newPosition) {
        if (newPosition > 40) {
            player.setPosition(newPosition - 40);
            passStart(player);
        } else {
            player.setPosition(newPosition);
        }

        return player;
    }

    public Player buyProperty(PropertyField propertyField, Player p) throws InsufficientMoneyException {
        if (propertyField.getOwnedBy() == null && propertyField.getOwnedBy() != p) {
            p.decreaseMoney(propertyField.getBuyValue());
            List<PropertyField> properties = p.getPropertyFields();
            properties.add(propertyField);
            p.setPropertyFields(properties);

            Street street = propertyField.getStreet();
            int propertyInStreetAmount = street.getPropertyAmount();
            int counter = 0;
            for (PropertyField properFieldOfPlayer : properties) {
                if (properFieldOfPlayer.getStreet() == street) {
                    counter++;
                }
            }
            if (counter == propertyInStreetAmount) {
                List<Street> streets = p.getStreets();
                streets.add(propertyField.getStreet());
                p.setStreets(streets);
            }

            //  if (p.getPropertyFields().contains()){

        }

        return p;
    }


    public void buildHouse(PropertyField propertyField, Player p) throws InsufficientMoneyException {
        if (p.getStreets().contains(propertyField.getStreet())) {
            if (propertyField instanceof StreetField) {
                int maxDifference = 0;
                int houseAmount = ((StreetField) propertyField).getHousesAmount() + 1;

                for (PropertyField propertyFieldInStreet : p.getPropertyFields().stream().filter(f -> f.getStreet() == propertyField.getStreet()).collect(Collectors.toList())) {
                    if (propertyFieldInStreet instanceof StreetField) {
                        if (houseAmount > ((StreetField) propertyFieldInStreet).getHousesAmount()) {
                            int tempHouseAmount = Math.abs(houseAmount - ((StreetField) propertyFieldInStreet).getHousesAmount());
                            if (tempHouseAmount > maxDifference) {
                                maxDifference = tempHouseAmount;
                            }
                        }
                    }
                }

                if (maxDifference < 2) {
                    StreetField streetField = (StreetField) propertyField;
                    if (streetField.getHousesAmount() < 5) {
                        p.decreaseMoney(streetField.getPriceHouseHotel());
                        streetField.setHousesAmount(streetField.getHousesAmount() + 1);
                    }
                }
            }
        }
        this.save(p);
    }

    public Game setMortgageOn(Field field, int playerId) throws FieldNotFoundException, PlayerNotFoundException {
        if (field instanceof PropertyField) {
            PropertyField propertyField = (PropertyField) field;
            Player player = this.getPlayer(playerId);
            if (player.getPropertyFields().contains(propertyField)) {
                if (!propertyField.isInMortgage()) {
                    propertyField.setInMortgage(true);
                    player.increaseMoney(propertyField.getMortgage());
                }
            }
        }
        return field.getGame();
    }


    public Game setMortgageOff(Field field, int playerId) throws PlayerNotFoundException {
        if (field instanceof PropertyField) {
            PropertyField propertyField = (PropertyField) field;
            Player player = this.getPlayer(playerId);
            if (player.getPropertyFields().contains(propertyField)) {
                if (propertyField.isInMortgage()) {
                    player.decreaseMoney(propertyField.getMortgage());
                    propertyField.setInMortgage(false);
                }
            }
        }
        return field.getGame();
    }
}

    /*

//    public void increaseMoney(int amount, Player p) {
//        p.setMoney(p.getMoney() + amount);
//        playerRepository.save(p);
//    }
//
//    public void decreaseMoney(int amount, Player p) throws InsufficientMoneyException {
//        if (p.getMoney() < amount) {
//            throw new InsufficientMoneyException();
//        }
//        p.setMoney(p.getMoney() - amount);
//
//        playerRepository.save(p);
//    }










    public void sellHouse(Property property, Player p){
        if (property instanceof BuildProperty) {
            BuildProperty buildProperty = (BuildProperty) property;
            if (buildProperty.getHousesAmount() > 0) {
                this.increaseMoney(((BuildProperty) property).getPriceHouseHotel()/2, p);
                buildProperty.setHousesAmount(buildProperty.getHousesAmount() - 1);
            }
        }
    }


    // Moet beide worden uitgevoerd?
    @Transactional
    public void payRent(Player playerFrom, Player playerTo, int amount) throws InsufficientMoneyException {
        this.decreaseMoney(amount, playerFrom);
        this.increaseMoney(amount, playerTo);
    }


   */


