package com.example.MonopolyBackend.services;

import com.example.MonopolyBackend.exceptions.FieldNotFoundException;
import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.fields.Field;
import com.example.MonopolyBackend.repositories.FieldRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author Thomas Somers
 * @version 1.0 31/01/2019 16:03
 */

@Service
@Transactional
public class FieldService {

    private final FieldRepository fieldRepository;

    public FieldService(FieldRepository fieldRepository) {
        this.fieldRepository = fieldRepository;
    }

    public Field get(int position, int gameId) throws FieldNotFoundException {
        Optional<Field> field = fieldRepository.findByPositionAndGameId(position, gameId);
        if (field.isPresent()) {
            return field.get();
        }
        throw new FieldNotFoundException("Field not found with position: " + position + " and gameId: " + gameId);
    }

    public Collection<Field> getFields() {
        return fieldRepository.findAll();
    }

    public Field save(Field field) {
        return fieldRepository.save(field);
    }

    public Game receivePlayer(Player player, int gameId) throws FieldNotFoundException {
        Field field = this.get(player.getPosition(), gameId);
        return field.receivePlayer(player);
    }

    public void saveAll(List<Field> fields) {
        fieldRepository.saveAll(fields);
    }

    public Collection<Field> getFieldsByGameId(int id) {
        return fieldRepository.findAllByGameId(id);
    }

}
