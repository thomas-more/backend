package com.example.MonopolyBackend.controllers;

import com.example.MonopolyBackend.dto.InviteDTO;
import com.example.MonopolyBackend.dto.UserDTO;
import com.example.MonopolyBackend.exceptions.UserNotFoundException;
import com.example.MonopolyBackend.model.*;
import com.example.MonopolyBackend.repositories.ConfirmationTokenRepository;
import com.example.MonopolyBackend.repositories.UserRepository;
import com.example.MonopolyBackend.security.CustomAuthenticationManager;
import com.example.MonopolyBackend.security.JwtTokenProvider;
import com.example.MonopolyBackend.services.EmailService;
import com.example.MonopolyBackend.services.InviteService;
import com.example.MonopolyBackend.services.UserService;
import net.bytebuddy.utility.RandomString;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "${clientWebPage}")
public class  UserRestController {
    private UserService userService;
    private EmailService emailService;
    private final ModelMapper modelMapper;
    private ConfirmationTokenRepository confirmationTokenRepository;
    private UserRepository userRepository;
    private CustomAuthenticationManager authenticationManager;
    private JwtTokenProvider tokenProvider;
    @Value("${clientWebPage}")
    private String webClientUrl;
    private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);
    private InviteService inviteService;

    public UserRestController(UserService userService, EmailService emailService, ModelMapper modelMapper, ConfirmationTokenRepository confirmationTokenRepository, UserRepository userRepository, CustomAuthenticationManager authenticationManager, JwtTokenProvider tokenProvider, InviteService inviteService) {
        this.userService = userService;
        this.emailService = emailService;
        this.modelMapper = modelMapper;
        this.confirmationTokenRepository = confirmationTokenRepository;
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.inviteService = inviteService;
    }

    @GetMapping("/users/{username}")
    public ResponseEntity<UserDTO> loadAllProperties(@PathVariable String username) throws UserNotFoundException {
        User user = userService.getUser(username);

        return new ResponseEntity<>(modelMapper.map(user, UserDTO.class), HttpStatus.OK);
    }

    @PostMapping("/users/register")
    public ResponseEntity<UserDTO> register(@RequestBody User user) {
        LOGGER.info("register received");
        if (userService.userExists(user.getUsername())) {
            return null;
        }
        user.setConfirmed(false);
        if (user.getRole() == null) {
            user.setRole(UserRole.NORMAL_USER);
        }
        User returnedUser = userService.createUser(user);

        ConfirmationToken confirmationToken = new ConfirmationToken(returnedUser);

        confirmationTokenRepository.save(confirmationToken);
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject("Monopoly registratie");
        mailMessage.setText(String.format("Hello %s\n\nThanks for registering on our monopoly game!\nTo confirm you account click on this link: " +
                "http://localhost:8080/api/users/confirm?token=%s\n\nKind regards", returnedUser.getUsername(), confirmationToken.getConfirmationToken()));
        emailService.sendEmail(mailMessage);
        return new ResponseEntity<>(modelMapper.map(returnedUser, UserDTO.class), HttpStatus.OK);
    }


    @GetMapping("/users/confirm")
    public ResponseEntity<Object> confirmUser(@RequestParam("token") String confirmationToken, HttpServletResponse response) throws UserNotFoundException, URISyntaxException {
        LOGGER.info("confirm received");
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
        URI redirect = new URI(webClientUrl + "/login");

        if (token != null) {
            User user = userRepository.findByUsernameIgnoreCase(token.getUser().getUsername());
            userService.confirmUser(user.getId());
        } else {
            redirect = new URI(webClientUrl + "/error");
            return null;
        }
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(redirect);
        return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
    }

    @PutMapping("/users/update")
    public ResponseEntity<UserDTO> updateUser(@RequestBody User user) {
        if (userService.userCheckDuplicate(user)) {
            return null;
        }
        User returnedUser = null;
        try {
            returnedUser = userService.changeUser(user);
        } catch (UserNotFoundException | ConstraintViolationException e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(modelMapper.map(returnedUser, UserDTO.class), HttpStatus.OK);
    }

    @PostMapping("/users/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody User user) {
        LOGGER.info("login received");

        try {

            if (!userService.getUser(user.getUsername()).getRole().equals(UserRole.NORMAL_USER)) {

                return null;
            }
        } catch (UserNotFoundException e) {

            return null;
        }
        UsernamePasswordAuthenticationToken u = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

        Authentication authentication = authenticationManager.authenticate(u);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt;
        try {
            jwt = tokenProvider.generateToken(authentication);
        } catch (UserNotFoundException e) {
            return null;
        }
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping("/users/sociallogin")
    public ResponseEntity<?> authenticateUser(@RequestBody SocialUser socialUser) {
        LOGGER.info("social login received");
        if (!userService.userExists(socialUser.getName())) {
            register(new User(socialUser.getName(), socialUser.getEmail(), RandomString.make(8), false, UserRole.FACEBOK_USER));
        }
        UsernamePasswordAuthenticationToken u = new UsernamePasswordAuthenticationToken(socialUser.getName(), null);
        Authentication authentication = authenticationManager.authenticate(u);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = null;
        try {
            jwt = tokenProvider.generateToken(authentication);
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @GetMapping("/users/invites/{username}")
    public ResponseEntity<Collection<InviteDTO>> getAllInvites(@PathVariable String username) throws UserNotFoundException {
        Collection<Invite> invites = inviteService.getInvites(userService.getUser(username));
        Collection<InviteDTO> inviteDTOS = new ArrayList<>();
        invites.forEach(i -> inviteDTOS.add(modelMapper.map(i, InviteDTO.class)));
        return new ResponseEntity<>(inviteDTOS, HttpStatus.OK);
    }

    @GetMapping("/user/{username}")
    public ResponseEntity<UserDTO> getUser(@PathVariable String username) throws UserNotFoundException {
       return new ResponseEntity<>(modelMapper.map(userService.getUser(username), UserDTO.class), HttpStatus.OK);
    }
}

