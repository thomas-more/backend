package com.example.MonopolyBackend.controllers;

import com.example.MonopolyBackend.dto.FieldDTO;
import com.example.MonopolyBackend.exceptions.GameNotFoundException;
import com.example.MonopolyBackend.model.fields.Field;
import com.example.MonopolyBackend.services.FieldService;
import com.example.MonopolyBackend.services.GameService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;


/**
 * @author Thomas Somers
 * @version 1.0 31/01/2019 16:01
 */

@RestController
@RequestMapping("/fields")
@CrossOrigin(origins = "${clientWebPage}")
public class FieldController {

    private final FieldService fieldService;
    private final ModelMapper modelMapper;
    private final GameService gameService;

    public FieldController(FieldService fieldService, ModelMapper modelMapper, GameService gameService) {
        this.fieldService = fieldService;
        this.modelMapper = modelMapper;
        this.gameService = gameService;
    }

    @GetMapping("/get/{gameId}")
    public ResponseEntity<Collection<FieldDTO>> loadAllProperties(@PathVariable int gameId) throws GameNotFoundException {
        Collection<Field> fields = fieldService.getFieldsByGameId(gameId);
        Collection<FieldDTO> fieldDTOS = new ArrayList<>();
        fields.forEach(f -> fieldDTOS.add(modelMapper.map(f, FieldDTO.class)));
        return new ResponseEntity<>(fieldDTOS, HttpStatus.OK);
    }
}
