package com.example.MonopolyBackend.controllers;

import com.example.MonopolyBackend.dto.GameDTO;
import com.example.MonopolyBackend.exceptions.FieldNotFoundException;
import com.example.MonopolyBackend.exceptions.GameNotFoundException;
import com.example.MonopolyBackend.exceptions.PlayerNotFoundException;
import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.services.FieldService;
import com.example.MonopolyBackend.services.GameService;
import com.example.MonopolyBackend.services.PlayerService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.modelmapper.ModelMapper;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Thomas Somers
 * @version 1.0 18/02/2019 10:56
 */

@RestController
@RequestMapping("/player")
@CrossOrigin(origins = "${clientWebPage}")
public class PlayerController {

    private ModelMapper modelMapper;
    private PlayerService playerService;
    private FieldService fieldService;
    private GameService gameService;
    private final SimpMessagingTemplate template;

    public PlayerController(ModelMapper modelMapper, PlayerService playerService, FieldService fieldService, GameService gameService, SimpMessagingTemplate template) {
        this.modelMapper = modelMapper;
        this.playerService = playerService;
        this.fieldService = fieldService;
        this.gameService = gameService;
        this.template = template;
    }

    @MessageMapping("/pickPawn")
    public void pickPawn(String message) throws GameNotFoundException, PlayerNotFoundException {
        JsonObject jsonObject = new JsonParser().parse(message).getAsJsonObject();
        int gameId = jsonObject.get("gameId").getAsInt();
        String pawn = jsonObject.get("pawn").toString().substring(1,jsonObject.get("pawn").toString().length()-1);

        playerService.setPawn(jsonObject.get("playerId").getAsInt(), pawn);
        this.template.convertAndSend("/lobby" + gameId, modelMapper.map(gameService.findGameById(gameId),GameDTO.class));
    }

    @MessageMapping("/roll")
    public void roll(String message) throws FieldNotFoundException, PlayerNotFoundException {
        JsonObject jsonObject = new JsonParser().parse(message).getAsJsonObject();
        Player player = playerService.movePlayer(playerService.getPlayer(jsonObject.get("playerId").getAsInt()));
        Game game = fieldService.receivePlayer(player,jsonObject.get("gameId").getAsInt());
        gameService.save(game);
        System.out.println("Rollen met speler: " + playerService.getPlayer(jsonObject.get("playerId").getAsInt()).getName());
        System.out.println("Nieuwe positie: " + playerService.getPlayer(jsonObject.get("playerId").getAsInt()).getPosition());
        this.template.convertAndSend("/lobby"+game.getId(),modelMapper.map(game,GameDTO.class));
    }


/*
    @PutMapping("/buy/{id}")
    public ResponseEntity<PlayerDTO> buyProperty(@PathVariable int id) throws PlayerNotFoundException, FieldNotFoundException, InsufficientMoneyException {
        Player player = playerService.movePlayer(playerService.getPlayer(id));
        Field field = fieldService.get(player.getPosition());
        Player playerout = playerService.buyProperty((PropertyField) field, player);
        playerService.save(playerout);

        return new ResponseEntity<>(modelMapper.map(playerout, PlayerDTO.class), HttpStatus.OK);

    }

    @PostMapping("/build/{playerId}")
    public ResponseEntity<PlayerDTO> buildHouse(@PathVariable int playerId,@RequestParam int fieldId) throws FieldNotFoundException, PlayerNotFoundException, InsufficientMoneyException {
        Player player = playerService.getPlayer(playerId);
        PropertyField propertyField = (PropertyField) fieldService.get(fieldId);
        playerService.buildHouse(propertyField, player);

        return new ResponseEntity<>(modelMapper.map(player, PlayerDTO.class), HttpStatus.OK);
    }

    @PutMapping("/mortgage/on/{playerId}")
    public ResponseEntity<GameDTO> setMortgageOn(@PathVariable int playerId,@RequestParam int fieldId) throws PlayerNotFoundException, FieldNotFoundException {
        Field field = fieldService.get(fieldId);
        Game game = playerService.setMortgageOn(field, playerId);
        gameService.save(game);

        return new ResponseEntity<>(modelMapper.map(game, GameDTO.class), HttpStatus.OK);
    }

    @PutMapping("/mortgage/off/{playerId}")
    public ResponseEntity<GameDTO> setMortgageOff(@PathVariable int playerId, @RequestParam int fieldId) throws FieldNotFoundException, PlayerNotFoundException {
        Field field = fieldService.get(fieldId);
        Game game = playerService.setMortgageOff(field, playerId);
        gameService.save(game);

        return new ResponseEntity<>(modelMapper.map(game, GameDTO.class), HttpStatus.OK);
    }
*/
}
