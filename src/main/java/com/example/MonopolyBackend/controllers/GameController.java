package com.example.MonopolyBackend.controllers;

import com.example.MonopolyBackend.dto.GameDTO;
import com.example.MonopolyBackend.exceptions.*;
import com.example.MonopolyBackend.model.*;
import com.example.MonopolyBackend.model.cards.Card;
import com.example.MonopolyBackend.model.fields.Field;
import com.example.MonopolyBackend.model.fields.PropertyField;
import com.example.MonopolyBackend.services.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author Thomas Somers
 * @version 1.0 13/02/2019 8:51
 */

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "${clientWebPage}")
public class GameController {
    private ModelMapper modelMapper;
    private GameService gameService;
    private PlayerService playerService;
    private FieldService fieldService;
    private FieldFactory fieldFactory;
    private final SimpMessagingTemplate template;
    private CardService cardService;
    private CardFactory cardFactory;
    private UserService userService;
    private InviteService inviteService;
    private ChatMessageService chatMessageService;
    private Gson gson;


    public GameController(ModelMapper modelMapper, GameService gameService, PlayerService playerService, FieldService fieldService, SimpMessagingTemplate template, CardService cardService, UserService userService, InviteService inviteService, ChatMessageService chatMessageService) {
        this.modelMapper = modelMapper;
        this.gameService = gameService;
        this.playerService = playerService;
        this.template = template;
        this.cardService = cardService;
        this.userService = userService;
        this.inviteService = inviteService;
        this.chatMessageService = chatMessageService;
        this.gson = new Gson();
        this.cardFactory = new CardFactory();
        this.fieldFactory = new FieldFactory();
        this.fieldService = fieldService;
    }

    @MessageMapping("/game/create")
    public void createNewGame(String message) throws IOException, InvalidFormatException, GameNotFoundException {
        Game game = gameService.findGameById(Integer.parseInt(message));
        gameService.save(game);
        System.out.println("voor het maken");
        List<Field> fields = fieldFactory.generateFields(game);
        List<Card> cards = cardFactory.generateCards(game);
        System.out.println("na maken velden en kaarten");
        Collections.shuffle(cards);
        fieldService.saveAll(fields);
        cardService.saveAll(cards);
        System.out.println("na saven velden en kaarten");
        game.setFields(fields);
        game.setCards(cards);
        game.setStarted(true);
        game.getPlayers().forEach(player -> {
            try {
                User u = userService.getUser(player.getName());
                u.addPlayer(player);
                userService.save(u);
            } catch (UserNotFoundException e) {
                e.printStackTrace();
            }
        });
        gameService.save(game);
        System.out.println("na saven game");
        this.template.convertAndSend("/lobby" + message,modelMapper.map(game, GameDTO.class));
    }


    @MessageMapping("/endTurn")
    public void endTurn(String message) throws GameNotFoundException {
        Game game = gameService.findGameById(Integer.valueOf(message));
        Game gameOut=gameService.setNextPlayerPlaying(game.getId());
        this.template.convertAndSend("/lobby" + Integer.valueOf(message),modelMapper.map(gameOut, GameDTO.class));
    }
    @MessageMapping("/buyProperty")
    public void buyProperty(String message) throws PlayerNotFoundException, FieldNotFoundException, InsufficientMoneyException, GameNotFoundException {
        Game game = gameService.findGameById(Integer.valueOf(message));
        Player p = game.getPlayerPlaying();
        Field field = fieldService.get(p.getPosition(),game.getId());

        playerService.buyProperty((PropertyField) field, p);
        Game gameOut= gameService.setFieldOwner(game);
        //gameService.save(gameOut);
        this.template.convertAndSend("/lobby" + Integer.valueOf(message),modelMapper.map(gameOut, GameDTO.class));
    }


    @PostMapping("/lobby/create")
    public ResponseEntity<GameDTO> createNewLobby(@RequestBody String username) throws UserNotFoundException {
        Player player =new Player(userService.getUser(username).getUsername() ,null);
        Game game = new Game(player);
        player.setGame(game);
        gameService.save(game);
        return new ResponseEntity<>(modelMapper.map(game, GameDTO.class), HttpStatus.CREATED);
    }

    @MessageMapping("/lobby/add")
    public void addPlayer(String message) throws GameNotFoundException, UserNotFoundException {
        System.out.println("speler toegevoegd!");
        JsonObject jsonObject = (new JsonParser().parse(message).getAsJsonObject());
        Game game = gameService.findGameById(jsonObject.get("gameId").getAsInt());
        //TODO: dog pawn deleten
        Player player =new Player(userService.getUser(jsonObject.get("userId").getAsLong()).getUsername() ,null);
        playerService.save(player);
        game.addPlayer(player);
        gameService.save(game);
        System.out.println("SENDING ADDDDDDDDDD");
        this.template.convertAndSend("/lobby" + jsonObject.get("gameId").getAsInt(),modelMapper.map(game, GameDTO.class));
    }

    @PostMapping("/lobby/invite")
    public ResponseEntity<GameDTO> mailInvite(@RequestBody String inviteParams) throws GameNotFoundException {
        //TODO email uniek maken :)
        JsonObject jsonObject = (new JsonParser().parse(inviteParams).getAsJsonObject());
        User user = null;
        Game game = null;
        try {
            user = userService.getUserByEmail(jsonObject.get("email").getAsString());
            game = gameService.findGameById(jsonObject.get("gameId").getAsInt());
        } catch (UserNotFoundException e) {
            System.out.println("USER NOT FOUND");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Invite invite =  new Invite(game,user, game.getPlayers().get(0).getName());
        inviteService.save(invite);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("game/{gameId}")
    public ResponseEntity<GameDTO> getGame(@PathVariable int gameId) throws GameNotFoundException {
        Game game = gameService.findGameById(gameId);
        return new ResponseEntity<>(modelMapper.map(game,GameDTO.class), HttpStatus.OK);
    }

    @MessageMapping("game/newmessage")
    public void  chat(String message) throws GameNotFoundException {
        JsonObject jsonObject = (new JsonParser().parse(message).getAsJsonObject());
        Game game = gameService.findGameById(jsonObject.get("gameId").getAsInt());
        ChatMessage chatMessage = new ChatMessage(jsonObject.get("player").getAsString(),jsonObject.get("chatMessage").getAsString());
        chatMessageService.save(chatMessage);
        game.addMessage(chatMessage);
        gameService.save(game);
        this.template.convertAndSend("/lobby" + jsonObject.get("gameId").getAsInt(),modelMapper.map(game, GameDTO.class));

    }
}
