package com.example.MonopolyBackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InviteDTO {
    private int gameId;
    private int userId;
    private String hostUser;
}
