package com.example.MonopolyBackend.dto;

import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @author Thomas Somers
 * @version 1.0 31/01/2019 16:05
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FieldDTO {

    @NotEmpty
    private int id;

    @NotEmpty
    private int position;

    @NotEmpty
    private String name;

    @NotEmpty
    private Street street;

    @NotEmpty
    private int buyValue;

    @NotEmpty
    private boolean isInMortgage;

    @NotEmpty
    private Player ownedBy;

    @NotEmpty
    private int housesAmount;

    @NotEmpty
    private int rentUnbuild;

    @NotEmpty
    private int rentOneHouses;

    @NotEmpty
    private int rentTwoHouses;

    @NotEmpty
    private int rentThreeHouses;

    @NotEmpty
    private int rentFourHouses;

    @NotEmpty
    private int rentHotel;

    @NotEmpty
    private int mortgage;

    @NotEmpty
    private int priceHouseHotel;

    @NotEmpty
    private int priceOneUtility;

    @NotEmpty
    private int priceTwoUtilities;

    @NotEmpty
    private int priceOneStation;

    @NotEmpty
    private int priceTwoStations;

    @NotEmpty
    private int priceThreeStations;

    @NotEmpty
    private int priceFourStations;

    @NotEmpty
    private GameDTO gameDTO;

}
