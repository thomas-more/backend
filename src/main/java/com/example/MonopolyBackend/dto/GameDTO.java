package com.example.MonopolyBackend.dto;

import com.example.MonopolyBackend.model.ChatMessage;
import com.example.MonopolyBackend.model.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author Thomas Somers
 * @version 1.0 18/02/2019 9:55
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameDTO {

    @NotEmpty
    private int parkingPot;

    @NotEmpty
    private List<PlayerDTO> players;

    @NotEmpty
    private List<FieldDTO> fields;

    @NotEmpty
    private Player playerPlaying;

    @NotEmpty
    private List<ChatMessageDTO> messages;

    private boolean started;
    private int id;
    private int maxTurnTime;

}
