package com.example.MonopolyBackend.dto;

import com.example.MonopolyBackend.model.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessageDTO {
    String player;
    String chatMessage;
}
