package com.example.MonopolyBackend.dto;

import com.example.MonopolyBackend.model.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private long id;
    private String username;
    private String email;
    private String password;
    private boolean confirmed;
    private Player[] players;
}
