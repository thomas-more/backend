package com.example.MonopolyBackend.dto;

import com.example.MonopolyBackend.model.Street;
import com.example.MonopolyBackend.model.cards.Card;
import com.example.MonopolyBackend.model.fields.PropertyField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Thomas Somers
 * @version 1.0 18/02/2019 9:57
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerDTO {
    private int id;
    private String name;
    private String pawn;
    private int position;
    private int money;
    private boolean isInJail;

  /*  private List<Street> streets;
    private List<PropertyField> propertyFields;
    private List<Card> cards;*/
}
