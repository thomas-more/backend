package com.example.MonopolyBackend.repositories;

import com.example.MonopolyBackend.model.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatMessageRepository extends JpaRepository<ChatMessage, Integer> {
    
}
