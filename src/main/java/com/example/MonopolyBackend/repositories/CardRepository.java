package com.example.MonopolyBackend.repositories;

import com.example.MonopolyBackend.model.cards.Card;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRepository extends JpaRepository<Card, Integer> {

}
