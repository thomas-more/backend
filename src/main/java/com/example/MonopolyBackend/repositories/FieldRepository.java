package com.example.MonopolyBackend.repositories;

import com.example.MonopolyBackend.model.fields.Field;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Thomas Somers
 * @version 1.0 31/01/2019 16:33
 */
public interface FieldRepository extends JpaRepository<Field, Integer> {
    List<Field> findAllByGameId(int id);
    Optional<Field> findByPositionAndGameId(int position, int gameId);
}
