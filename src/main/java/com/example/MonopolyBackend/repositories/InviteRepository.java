package com.example.MonopolyBackend.repositories;

import com.example.MonopolyBackend.model.Invite;
import com.example.MonopolyBackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InviteRepository extends JpaRepository<Invite,Integer> {
    List<Invite> findAllByUser(User user);
}
