package com.example.MonopolyBackend.repositories;

import com.example.MonopolyBackend.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player,Integer> {

}
