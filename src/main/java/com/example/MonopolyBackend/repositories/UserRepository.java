package com.example.MonopolyBackend.repositories;


import com.example.MonopolyBackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
    User findByUsernameIgnoreCase(String username);
    User findByEmail(String email);
    boolean existsUserByUsername(String username);
}
