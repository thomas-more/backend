package com.example.MonopolyBackend.repositories;

import com.example.MonopolyBackend.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Thomas Somers
 * @version 1.0 13/02/2019 8:54
 */
public interface GameRepository extends JpaRepository<Game,Integer> {

}
