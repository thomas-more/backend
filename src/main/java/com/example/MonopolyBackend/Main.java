package com.example.MonopolyBackend;

import com.example.MonopolyBackend.exceptions.InsufficientMoneyException;
import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import com.example.MonopolyBackend.model.fields.Field;
import com.example.MonopolyBackend.model.fields.StreetField;
import com.example.MonopolyBackend.services.FieldService;
import com.example.MonopolyBackend.services.PlayerService;

import java.util.List;

/**
 * @author Thomas Somers
 * @version 1.0 13/02/2019 9:03
 */
public class Main {
    //GEBEURD IN LOBBY!!!!!!!!!!!


    /*
    public static void main(String[] args) throws InterruptedException, InsufficientMoneyException {


        PlayerService playerService = new PlayerService();

        // Spelers toevoegen, later via controller
        Player speler1 = new Player(1,"Speler1", "p1");
        Player speler2 = new Player(2,"Speler2", "p2");
        Player speler3 = new Player(3,"Speler3", "p3");
        playerService.addPlayer(speler1);
        playerService.addPlayer(speler2);
        playerService.addPlayer(speler3);

        BuildProperty p1 = new BuildProperty(){};
        p1.setPriceHouseHotel(5000);
        p1.setBuyValue(10000);

        BuildProperty p2 = new BuildProperty(){};
        p2.setPriceHouseHotel(725);
        p2.setBuyValue(4000);

        BuildProperty p3 = new BuildProperty(){};
        p3.setPriceHouseHotel(564);
        p3.setBuyValue(12000);

        // Simulatie mogelijk maken
        List<Player> players = playerService.getPlayers();
        Game game = new Game(60, players);

        boolean endGame =false;
        Player playerPlaying = game.getPlayerPlaying();

        while (!endGame) {
            playerService.movePlayer(playerPlaying);

            if (playerPlaying.getPosition() == 8){
                playerService.buyProperty(p1,playerPlaying);
            }
            if (playerPlaying.getPosition() == 25){
                playerService.buyProperty(p2,playerPlaying);
            }
            if (playerPlaying.getPosition() == 35){
                playerService.buyProperty(p3,playerPlaying);
            }


            playerPlaying = game.getNextPlayer();
            System.out.println(playerPlaying);
            Thread.sleep(500);

        }
    }
    */
}
