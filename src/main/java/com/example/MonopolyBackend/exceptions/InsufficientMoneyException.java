package com.example.MonopolyBackend.exceptions;

/**
 * @author Thomas Somers
 * @version 1.0 13/02/2019 9:59
 */
public class InsufficientMoneyException extends Exception {
}
