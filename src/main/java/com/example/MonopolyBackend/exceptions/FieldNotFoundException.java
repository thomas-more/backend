package com.example.MonopolyBackend.exceptions;

/**
 * @author Thomas Somers
 * @version 1.0 31/01/2019 16:06
 */
public class FieldNotFoundException extends Exception {

    public FieldNotFoundException(String message) {
        super(message);
    }
}
