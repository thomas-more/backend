package com.example.MonopolyBackend.exceptions;

/**
 * @author Thomas Somers
 * @version 1.0 18/02/2019 11:09
 */
public class GameNotFoundException extends Exception {
}
