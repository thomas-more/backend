package com.example.MonopolyBackend.security;

import com.example.MonopolyBackend.exceptions.UserNotFoundException;
import com.example.MonopolyBackend.model.User;
import com.example.MonopolyBackend.services.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationManager implements AuthenticationManager {

    private UserService userService;
    private PasswordEncoder passwordEncoder;

    public CustomAuthenticationManager(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getPrincipal().toString();
        String password = null;
        if(authentication.getCredentials()!=null){
            password = authentication.getCredentials().toString();
        }


        try {
            User user = userService.getUser(username);
            if (password != null && !passwordEncoder.matches(password, user.getPassword())) {
                System.out.println(password + "\n" + user.getPassword());
                throw new BadCredentialsException(":(");
            }
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }
        return new UsernamePasswordAuthenticationToken(username,password);
    }
}
