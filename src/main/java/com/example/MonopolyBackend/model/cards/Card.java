package com.example.MonopolyBackend.model.cards;

import com.example.MonopolyBackend.model.Game;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Thomas Somers
 * @version 1.0 12/02/2019 17:14
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING)
public abstract class Card {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String type;
    @OneToOne
    private Game game;

    public Card(String name, Game game,String type) {
        this.name = name;
        this.game = game;
        this.type = type;
    }
}
