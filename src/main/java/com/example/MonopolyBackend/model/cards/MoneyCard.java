package com.example.MonopolyBackend.model.cards;

import com.example.MonopolyBackend.model.Game;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@Data
@NoArgsConstructor
@DiscriminatorValue("MoneyCard")
public class MoneyCard extends Card{
    private int moneyChange;

    public MoneyCard(String name, Game game,String type, int moneyChange) {
        super(name, game,type);
        this.moneyChange = moneyChange;
    }
}
