package com.example.MonopolyBackend.model.cards;

import com.example.MonopolyBackend.model.Game;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@Data
@NoArgsConstructor
@DiscriminatorValue("AssesmentCard")
public class AssesmentCard extends Card{
    private int housePrice;


    public AssesmentCard(String name, Game game,String type, int housePrice) {
        super(name, game,type);
        this.housePrice = housePrice;
    }
}
