package com.example.MonopolyBackend.model.cards;

import com.example.MonopolyBackend.model.Game;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@Data
@NoArgsConstructor
@DiscriminatorValue("RelMoveCard")
public class RelMoveCard extends Card{
    private int moveXFields;

    public RelMoveCard(String name, Game game,String type, int moveXFields) {
        super(name, game,type);
        this.moveXFields = moveXFields;
    }
}
