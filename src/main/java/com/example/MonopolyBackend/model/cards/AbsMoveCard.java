package com.example.MonopolyBackend.model.cards;

import com.example.MonopolyBackend.model.Game;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@Data
@NoArgsConstructor
@DiscriminatorValue("AbsMoveCard")
public class AbsMoveCard extends Card {
    private int newPosition;

    public AbsMoveCard(String name, Game game,String type, int newPosition) {
        super(name, game,type);
        this.newPosition = newPosition;
    }
}
