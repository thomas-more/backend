package com.example.MonopolyBackend.model;


public class SocialUser {
    private String id;
    private String name;
    private String email;
    private String token;
    private String provider;
    private String image;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getToken() {
        return token;
    }

    public String getProvider() {
        return provider;
    }

    public SocialUser(){

    };
}
