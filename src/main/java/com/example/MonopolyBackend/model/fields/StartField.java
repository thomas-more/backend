package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:40
 */

@Data
@Entity
@NoArgsConstructor
@DiscriminatorValue("StartField")
public class StartField extends CornerField {

    @Override
    public Game receivePlayer(Player player) {
        return super.getGame();
    }

    public StartField(String name, int position, Street street, Game game) {
        super(name, position, street, game);
    }
}
