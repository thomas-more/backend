package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:42
 */

@Data
@Entity
@NoArgsConstructor
@DiscriminatorValue("ToPrisonField")
public class ToPrisonField extends CornerField {

    @Override
    public Game receivePlayer(Player player) {
        //TODO: nadenken over prison
        player.setInJail(true);
        player.setPosition(11);
    //    Map<Player, Integer> prison = super.getGame().getPrison();
    //    prison.put(player,3);
        return super.getGame();
    }

    public ToPrisonField(String name, int position, Street street, Game game) {
        super(name, position, street, game);
    }
}
