package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.HashMap;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:41
 */

@Data
@Entity
@NoArgsConstructor
@DiscriminatorValue("PrisonField")
public class PrisonField extends CornerField {


    @Override
    public Game receivePlayer(Player player) {
        return super.getGame();
    }

    public PrisonField(String name, int position, Street street, Game game) {
        super(name, position, street, game);
    }
}
