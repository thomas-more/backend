package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:36
 */

@Entity
@Data
@NoArgsConstructor
@DiscriminatorValue("StationField")
public class StationField extends PropertyField {

    private int priceOneStation;
    private int priceTwoStations;
    private int priceThreeStations;
    private int priceFourStations;

    public StationField(String name, int position, Street street, Game game, int buyValue, int mortgage, boolean isInMortgage, Player ownedBy, int priceOneStation, int priceTwoStations, int priceThreeStations, int priceFourStations) {
        super(name, position, street, game, buyValue, mortgage, isInMortgage, ownedBy);
        this.priceOneStation = priceOneStation;
        this.priceTwoStations = priceTwoStations;
        this.priceThreeStations = priceThreeStations;
        this.priceFourStations = priceFourStations;
    }

    @Override
    public Game receivePlayer(Player player) {
        //TODO: Field kunnen kopen indien nog niet in bezit van iemand anders
        if (getOwnedBy() != null && getOwnedBy() != player && !isInMortgage()) {
            Player playerReceiving = getOwnedBy();
            List<PropertyField> fields = playerReceiving.getPropertyFields();
            int stationCounter = 0;

            for (Field field : fields) {
                if (field instanceof StationField) {
                    stationCounter++;
                }
            }

            switch (stationCounter) {
                case 1:
                    player.decreaseMoney(priceOneStation);
                    playerReceiving.increaseMoney(priceOneStation);
                    break;
                case 2:
                    player.decreaseMoney(priceTwoStations);
                    playerReceiving.increaseMoney(priceTwoStations);
                    break;
                case 3:
                    player.decreaseMoney(priceThreeStations);
                    playerReceiving.increaseMoney(priceThreeStations);
                    break;
                case 4:
                    player.decreaseMoney(priceFourStations);
                    playerReceiving.increaseMoney(priceFourStations);
                    break;
            }
        }
        return super.getGame();

    }
}
