package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:26
 */

@Data
@Entity
@NoArgsConstructor
public abstract class PropertyField extends Field {
    private int buyValue;
    private int mortgage;
    private boolean isInMortgage;
    @OneToOne
    private Player ownedBy;

    public PropertyField(int id, int position,String name, Street street, int buyValue, int mortgage, boolean isInMortgage, Player ownedBy, Game game) {
        super(id, name,position,street, game);
        this.buyValue = buyValue;
        this.mortgage = mortgage;
        this.isInMortgage = isInMortgage;
        this.ownedBy = ownedBy;
    }

    public PropertyField(String name, int position, Street street, Game game, int buyValue, int mortgage, boolean isInMortgage, Player ownedBy) {
        super(name, position, street, game);
        this.buyValue = buyValue;
        this.mortgage = mortgage;
        this.isInMortgage = isInMortgage;
        this.ownedBy = ownedBy;
    }
}
