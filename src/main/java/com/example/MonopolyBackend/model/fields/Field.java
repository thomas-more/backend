package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:24
 */

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "FIELDS")
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING)
public abstract class Field {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private int position;
    @OneToOne
    private Street street;
    @OneToOne
    private Game game;

    public abstract Game receivePlayer(Player player);

    public Field(String name, int position, Street street, Game game) {
        this.name = name;
        this.position = position;
        this.street = street;
        this.game = game;
    }
}
