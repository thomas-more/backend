package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:41
 */

@Data
@Entity
@NoArgsConstructor
@DiscriminatorValue("FreeParkingField")
public class FreeParkingField extends CornerField {

    @Override
    public Game receivePlayer(Player player) {
        player.increaseMoney(super.getGame().getParkingPot());
        super.getGame().setParkingPot(0);
        return super.getGame();
    }

    public FreeParkingField(String name, int position, Street street, Game game) {
        super(name, position, street, game);
    }
}
