package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import com.example.MonopolyBackend.model.cards.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:38
 */

@Entity
@Data
@NoArgsConstructor
@DiscriminatorValue("DrawableCardfield")
public class DrawableCardField extends ExtraField {

    public DrawableCardField(String name, int position, Street street, Game game) {
        super(name, position, street, game);
    }

    @Override
    public Game receivePlayer(Player player) {
        Game game = super.getGame();
        List<Card> cards = game.getCards();

        Card card;
        int counter = 0;


        if (super.getName().equals("Algemeen Fonds")) {
            do {
                card = cards.get(counter);
                counter++;
            } while (card.getType().equals("Chance"));

        } else {
            do {
                card = cards.get(counter);
                counter++;
            } while (card.getType().equals("CommunityChest"));

        }


        if (card instanceof AbsMoveCard) {
            player.setPosition(((AbsMoveCard) card).getNewPosition());
        } else if (card instanceof RelMoveCard) {
            player.setPosition(player.getPosition() + ((RelMoveCard) card).getMoveXFields());
        } else if (card instanceof MoneyCard) {
            player.setMoney(player.getMoney() + ((MoneyCard) card).getMoneyChange());
        } else if (card instanceof AssesmentCard) {
            int totalPay = 0;
            for (PropertyField property: player.getPropertyFields()) {
                if (property instanceof StreetField) {
                    int houseAmount = ((StreetField) property).getHousesAmount();
                    totalPay += (houseAmount * ((AssesmentCard) card).getHousePrice());
                }
            }
            player.setMoney(player.getMoney() - totalPay);
        }


        Collections.shuffle(cards);

        return super.getGame();
    }


}
