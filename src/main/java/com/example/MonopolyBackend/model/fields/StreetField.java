package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:33
 */

@Data
@Entity
@NoArgsConstructor
@DiscriminatorValue("StreetField")
public class StreetField extends PropertyField {

    private int rentUnbuild;
    private int rentOneHouses;
    private int rentTwoHouses;
    private int rentThreeHouses;
    private int rentFourHouses;
    private int rentHotel;
    private int priceHouseHotel;
    private int housesAmount;

    public StreetField(String name, int position, Street street, Game game, int buyValue, int mortgage, boolean isInMortgage, Player ownedBy, int rentUnbuild, int rentOneHouses, int rentTwoHouses, int rentThreeHouses, int rentFourHouses, int rentHotel, int priceHouseHotel, int housesAmount) {
        super(name, position, street, game, buyValue, mortgage, isInMortgage, ownedBy);
        this.rentUnbuild = rentUnbuild;
        this.rentOneHouses = rentOneHouses;
        this.rentTwoHouses = rentTwoHouses;
        this.rentThreeHouses = rentThreeHouses;
        this.rentFourHouses = rentFourHouses;
        this.rentHotel = rentHotel;
        this.priceHouseHotel = priceHouseHotel;
        this.housesAmount = housesAmount;
    }

    @Override
    public Game receivePlayer(Player player) {
        if (getOwnedBy() != null && getOwnedBy() != player && !isInMortgage()) {
            Player playerReceiving = getOwnedBy();
            switch (this.housesAmount) {
                case 0:
                    if (playerReceiving.getStreets() != null) {
                        if (playerReceiving.getStreets().contains(super.getStreet())) {
                            player.decreaseMoney(this.rentUnbuild * 2);
                            playerReceiving.increaseMoney(this.rentUnbuild * 2);
                        }
                    } else {
                        player.decreaseMoney(this.rentUnbuild);
                        playerReceiving.increaseMoney(this.rentUnbuild);
                    }
                    break;
                case 1:
                    player.decreaseMoney(this.rentOneHouses);
                    playerReceiving.increaseMoney(this.rentOneHouses);
                    break;
                case 2:
                    player.decreaseMoney(this.rentTwoHouses);
                    playerReceiving.increaseMoney(this.rentTwoHouses);
                    break;
                case 3:
                    player.decreaseMoney(this.rentThreeHouses);
                    playerReceiving.increaseMoney(this.rentThreeHouses);
                    break;
                case 4:
                    player.decreaseMoney(this.rentFourHouses);
                    playerReceiving.increaseMoney(this.rentFourHouses);
                    break;
                case 5:
                    player.decreaseMoney(this.rentHotel);
                    playerReceiving.increaseMoney(this.rentHotel);
                    break;
            }
        }
        return super.getGame();
    }
}
