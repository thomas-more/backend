package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:35
 */

@Data
@Entity
@NoArgsConstructor
@DiscriminatorValue("FacilityField")
public class FacilityField extends PropertyField {

    private int priceOneUtility;
    private int priceTwoUtilities;

    public FacilityField(String name, int position, Street street, Game game, int buyValue, int mortgage, boolean isInMortgage, Player ownedBy, int priceOneUtility, int priceTwoUtilities) {
        super(name, position, street, game, buyValue, mortgage, isInMortgage, ownedBy);
        this.priceOneUtility = priceOneUtility;
        this.priceTwoUtilities = priceTwoUtilities;
    }

    @Override
    public Game receivePlayer(Player player) {
        //TODO: Field kunnen kopen indien nog niet in bezit van iemand anders
        if (getOwnedBy() != null && getOwnedBy() != player && !isInMortgage()) {
            Player playerReceiving = getOwnedBy();
            List<PropertyField> fields = playerReceiving.getPropertyFields();
            int facilityCounter = 0;

            for (Field field : fields) {
                if (field instanceof FacilityField) {
                    facilityCounter++;
                }
            }


            //TODO bedrag vermenigvuldigen met aantal gegooide ogen
            switch (facilityCounter) {
                case 1:
                    player.decreaseMoney(priceOneUtility);
                    playerReceiving.increaseMoney(priceOneUtility);
                    break;
                case 2:
                    player.decreaseMoney(priceTwoUtilities);
                    playerReceiving.increaseMoney(priceTwoUtilities);
                    break;
            }
        }
        return super.getGame();

    }
}
