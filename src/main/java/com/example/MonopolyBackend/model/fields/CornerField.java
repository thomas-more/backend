package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Street;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:40
 */

@Entity
@NoArgsConstructor
public abstract class CornerField extends ExtraField{

    public CornerField(String name, int position, Street street, Game game) {
        super(name, position, street, game);
    }
}
