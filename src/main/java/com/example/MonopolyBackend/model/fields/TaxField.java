package com.example.MonopolyBackend.model.fields;

import com.example.MonopolyBackend.model.Game;
import com.example.MonopolyBackend.model.Player;
import com.example.MonopolyBackend.model.Street;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Thomas Somers
 * @version 1.0 20/02/2019 8:40
 */

@Entity
@Data
@NoArgsConstructor
@DiscriminatorValue("TaxField")
public class TaxField extends ExtraField {


    @Override
    public Game receivePlayer(Player player) {
        player.decreaseMoney(2000);
        Game game = super.getGame();
        game.setParkingPot(game.getParkingPot()+2000);
        return super.getGame();

    }

    public TaxField(String name, int position, Street street, Game game) {
        super(name, position, street, game);
    }
}
