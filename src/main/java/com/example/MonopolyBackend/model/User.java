package com.example.MonopolyBackend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "USERS")
public class User {
    @Id
    @GeneratedValue
    private long id;
    @Column(unique = true)
    private String username;
    @Column
    private String email;
    @Column
    private String password;
    @Column
    private boolean confirmed;
    @Column
    private UserRole role;
    @Column
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Player> players;

    public User(String username, String email, String password, boolean confirmed) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.confirmed = confirmed;
    }

    public User(String username, String email, String password, boolean confirmed, UserRole role) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.confirmed = confirmed;
        this.role = role;
    }

    public void addPlayer(Player player){
        this.players.add(player);
    }
}