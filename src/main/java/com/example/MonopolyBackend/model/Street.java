package com.example.MonopolyBackend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Thomas Somers
 * @version 1.0 5/02/2019 16:04
 */


@NoArgsConstructor
@Data
@Entity
@Table(name = "STREETS")
public class Street {
    @Id
    private int id;
    private String name;
    private String color;
    private int propertyAmount;

    public Street(int id, String name, String color, int propertyAmount) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.propertyAmount = propertyAmount;
    }
}