package com.example.MonopolyBackend.model;

import com.example.MonopolyBackend.model.fields.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Thomas Somers
 * @version 1.0 12/03/2019 15:37
 */

public class FieldFactory {



    public List<Field> generateFields(Game game) throws IOException, InvalidFormatException {
        List<Field> fields = new ArrayList<>();

        Workbook workbook = WorkbookFactory.create(new ClassPathResource("files/fields.xlsx").getFile());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Field field = createField(row,game);
            fields.add(field);
        }

        return fields;
    }

    private Field createField(Row row, Game game) {
        Field field = null;
        String name = row.getCell(0).toString();
        int position = (int)row.getCell(24).getNumericCellValue();
        Street street = new Street((int)row.getCell(4).getNumericCellValue(), row.getCell(5).toString(), row.getCell(6).toString(), (int)row.getCell(7).getNumericCellValue());
        int buyValue = (int)row.getCell(2).getNumericCellValue();
        int mortgage = (int)row.getCell(16).getNumericCellValue();
        boolean isInMortgage = Boolean.getBoolean(row.getCell(3).toString());
        Player ownedby = null;
        int rentUnbuild = (int)row.getCell(10).getNumericCellValue();
        int rentOneHouses =(int)row.getCell(11).getNumericCellValue();
        int rentTwoHouses = (int)row.getCell(12).getNumericCellValue();
        int rentThreeHouses = (int)row.getCell(13).getNumericCellValue();
        int rentFourHouses =(int)row.getCell(14).getNumericCellValue();
        int rentHotel =(int)row.getCell(15).getNumericCellValue();
        int priceHouseHotel =(int)row.getCell(17).getNumericCellValue();
        int housesAmount = (int)row.getCell(9).getNumericCellValue();
        int priceOneUtility = (int)row.getCell(18).getNumericCellValue();
        int priceTwoUtilities = (int)row.getCell(19).getNumericCellValue();
        int priceOneStation = (int)row.getCell(20).getNumericCellValue();
        int priceTwoStations = (int)row.getCell(21).getNumericCellValue();
        int priceThreeStations = (int)row.getCell(22).getNumericCellValue();
        int priceFourStations = (int)row.getCell(23).getNumericCellValue();

        switch (row.getCell(1).toString()) {
            case "StartField":
                field = new StartField(name,position,street,game);
                break;
            case "StreetField":
                field = new StreetField(name, position, street, game, buyValue, mortgage, isInMortgage, ownedby, rentUnbuild, rentOneHouses, rentTwoHouses, rentThreeHouses, rentFourHouses, rentHotel, priceHouseHotel, housesAmount);
                break;
            case "DrawableCardfield":
                field = new DrawableCardField(name,position,street,game);
                break;
            case "TaxField":
                field = new TaxField(name,position,street,game);
                break;
            case "StationField":
                field = new StationField(name, position, street, game, buyValue, mortgage, isInMortgage, ownedby, priceOneStation, priceTwoStations, priceThreeStations, priceFourStations);
                break;
            case "PrisonField":
                field = new PrisonField(name,position,street,game);
                break;
            case "FacilityField":
                field = new FacilityField(name,position,street,game,buyValue,mortgage,isInMortgage,ownedby,priceOneUtility,priceTwoUtilities);
                break;
            case "FreeParkingField":
                field = new FreeParkingField(name,position,street,game);
                break;
            case "ToPrisonField":
                field = new ToPrisonField(name,position,street,game);
                break;

        }

        return field;
    }
}
