package com.example.MonopolyBackend.model;

import com.example.MonopolyBackend.model.cards.Card;
import com.example.MonopolyBackend.model.fields.Field;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Thomas Somers
 * @version 1.0 13/02/2019 8:33
 */

@Data
@Entity
@NoArgsConstructor
@Table(name = "game")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Game {
    @Id
    @GeneratedValue
    int id;
    private int nrPlayers;
    private int maxTurnTime;
    private boolean started;
    private int parkingPot;
    @OneToOne(cascade = {CascadeType.ALL})
    @JsonIgnoreProperties("game")
    private Player playerPlaying;
    private boolean endGame;
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnoreProperties("game")
    private List<Player> players;
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Card> cards; //Eventueel opsplitsen in kans en algemeen fonds
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Field> fields;
    private final int MAX_NR_PLAYERS = 4;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ChatMessage> messages;

    public Game(List<Player> players) {
        // int id wordt gegenereerd
        this.nrPlayers = players.size();
        this.cards = new ArrayList<>();
        this.fields = new ArrayList<>();
        this.parkingPot = 0;
        this.maxTurnTime = 60;
        this.players = players;
        this.playerPlaying = players.get(new Random().nextInt(players.size()));
        this.endGame = false;
        started = false;

        messages = new ArrayList<>();

    }

    public Game(Player player) {
        // int id wordt gegenereerd
        this.cards = new ArrayList<>();
        this.parkingPot = 0;
        this.maxTurnTime = 60;
        this.players = new ArrayList<>();
        this.players.add(player);
        this.nrPlayers = 1;
        this.endGame = false;
        started = false;
        playerPlaying = player;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
        this.nrPlayers++;
    }
    public void addMessage(ChatMessage chatMessage){
        messages.add(chatMessage);
    }

}

