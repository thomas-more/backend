package com.example.MonopolyBackend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "INVITES")
public class Invite {
    @Id
    @GeneratedValue
    private int id;
    @ManyToOne
    private Game game;
    @ManyToOne
    private User user;
    @Column
    private String hostUser;

    public Invite(Game game, User user, String hostUser) {
        this.game = game;
        this.user = user;
        this.hostUser = hostUser;
    }
}
