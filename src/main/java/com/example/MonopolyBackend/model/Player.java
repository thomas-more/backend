package com.example.MonopolyBackend.model;

import com.example.MonopolyBackend.model.cards.Card;
import com.example.MonopolyBackend.model.fields.PropertyField;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Thomas Somers
 * @version 1.0 12/02/2019 17:14
 */

@Data
@Entity
@NoArgsConstructor
public class Player {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String pawn;
    int position;
    int money;
    private boolean isInJail;


    @ManyToOne
    @JoinColumn(name = "game_id")
    @JsonIgnoreProperties(value = {"players", "playerPlaying"})
    private Game game;

    @JsonIgnore
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Street> streets;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JsonIgnore
    @Fetch(value = FetchMode.SUBSELECT)
    private List<PropertyField> propertyFields;
    //TODO: zonder gevangenisverlaatkaart mag dit weg

      @JsonIgnore
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Card> cards;

    public Player(String name, String pawn) {
        this.name = name;
        this.pawn = pawn;
        this.position = 1;
        this.money = 15000;
        this.propertyFields = new ArrayList<>();
        //this.cards = new ArrayList<>();
        this.isInJail = false;
    }

    public Player(int id, String name, String pawn) {
        this.id = id;
        this.name = name;
        this.pawn = pawn;
        this.position = 1;
        this.money = 15000;
        this.propertyFields = new ArrayList<>();
        //this.cards = new ArrayList<>();
        this.isInJail = false;
    }

    public void decreaseMoney(int amount) {
        this.money -= amount;
    }

    public void increaseMoney(int amount) {
        this.money += amount;
    }

}
