package com.example.MonopolyBackend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessage {
    @Id
    @GeneratedValue
    int id;
    String player;
    String chatMessage;


    public ChatMessage(String player, String chatMessage) {
        this.player = player;
        this.chatMessage = chatMessage;
    }
}
