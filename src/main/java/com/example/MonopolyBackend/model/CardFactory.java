package com.example.MonopolyBackend.model;

import com.example.MonopolyBackend.model.cards.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CardFactory {

    public CardFactory() { }

    public List<Card> generateCards(Game game) throws IOException, InvalidFormatException {
        List<Card> cards = new ArrayList<>();

        Workbook workbook = WorkbookFactory.create(new ClassPathResource("files/cards.xlsx").getFile());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Card card = createCard(row,game);
            cards.add(card);
        }

        return cards;
    }

    private Card createCard(Row row, Game game) {
        Card card = null;
        String name = row.getCell(0).toString();
        int newPosition = (int) row.getCell(2).getNumericCellValue();
        int relPosition = (int) row.getCell(3).getNumericCellValue();
        int moneyChange = (int) row.getCell(4).getNumericCellValue();
        int housePrice = (int) row.getCell(5).getNumericCellValue();
        String type = row.getCell(6).toString();
        switch (row.getCell(1).toString()) {
            case "AbsMoveCard":
                card = new AbsMoveCard(name,game,type,newPosition);
                break;
            case "RelMoveCard":
                card = new RelMoveCard(name,game,type,relPosition);
                break;
            case "MoneyCard":
                card= new MoneyCard(name,game,type,moneyChange);
                break;
            case "AssesmentCard":
                card= new AssesmentCard(name,game,type,housePrice);
                break;
        }

        return card;
    }

}
