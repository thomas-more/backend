package com.example.MonopolyBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonopolyBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonopolyBackendApplication.class, args);
	}

}

